# :coding: utf-8
# :copyright: Copyright (c) 2016 Martin Pengelly-Phillips

import sys

import pytest

import flattener


@pytest.mark.parametrize('input_list, expected', [
    ([], []),
    ([[[[]]]], []),
    ([1], [1]),
    ([1, 2, 3], [1, 2, 3]),
    ([1, [2, [3]]], [1, 2, 3]),
    ([[1, [2, [3]]]], [1, 2, 3]),
    ([[[[[[[[[1]]]]]]]]], [1])
], ids=[
    'empty list',
    'nested empty lists',
    'single non-nested entry',
    'multiple non-nested entries',
    'mixed non-nested and nested entries',
    'multiple nested entries',
    'deeply nested single entry'
])
def test_flatten(input_list, expected):
    '''Flatten input.'''
    flattened = flattener.flatten(input_list)
    assert flattened == expected


def test_flatten_beyond_recursion_limit():
    '''Flatten deeply nested input beyond recursion limit.'''
    recursion_limit = sys.getrecursionlimit()

    input_list = []
    current_list = input_list

    for index in range(recursion_limit + 1):
        next_list = []
        current_list.append(next_list)
        current_list = next_list

    else:
        current_list.append(1)

    try:
        flattened = flattener.flatten(input_list)
    except RuntimeError:
        raise AssertionError('Failed to flatten due to recursion depth limit.')

    assert flattened == [1]


@pytest.mark.parametrize('input_list, expected', [
    ('', ValueError),
    (None, ValueError)
], ids=[
    'non-list input',
    'null value'
])
def test_flatten_invalid_input(input_list, expected):
    '''Fail to flatten invalid input.'''
    with pytest.raises(expected):
        flattener.flatten(input_list)
