..
    :copyright: Copyright (c) 2016 Martin Pengelly-Phillips

.. _release/release_notes:

*************
Release Notes
*************

.. release:: 0.1.2
    :date: 2016-03-12

    .. change:: changed

        Removed leftover print statement.

.. release:: 0.1.1
    :date: 2016-03-12

    .. change:: changed

        Fix release notes and version bump because of PyPI rules.

.. release:: 0.1.0
    :date: 2016-03-12

    .. change:: new

        Initial release featuring :ref:`using/library` and
        :ref:`using/command_line` interfaces for flattening nested lists.
