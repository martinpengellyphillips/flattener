..
    :copyright: Copyright (c) 2016 Martin Pengelly-Phillips

.. _api_reference:

*************
API reference
*************

flattener
=========

.. automodule:: flattener

.. toctree::
    :maxdepth: 1
    :glob:

    */index
    *
