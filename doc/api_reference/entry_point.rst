..
    :copyright: Copyright (c) 2016 Martin Pengelly-Phillips

*********************
flattener.entry_point
*********************

.. automodule:: flattener.entry_point
